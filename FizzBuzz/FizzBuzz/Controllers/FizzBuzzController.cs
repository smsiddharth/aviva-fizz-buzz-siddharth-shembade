﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FizzBuzz.Models;
using PagedList.Mvc;
using PagedList;
using System.Configuration;
using System.Web.Mvc.ExpressionUtil;

namespace FizzBuzz.Controllers
{
     [HandleError(ExceptionType = typeof(InvalidOperationException), View = "InvalidInputErrorView")]
    public class FizzBuzzController : Controller
    {
        //
        // GET: /FizzBuzz/

        [HttpGet]
        public ActionResult Index()
        {
            return View("DefaultView");
        }


        /// <summary>
        /// Gets the FizzBuzz result for the user input.
        /// </summary>
        /// <param name="userInput">UserInput</param>
        /// <returns>ActionResult</returns>
        [HttpPost]
        public ActionResult GetFizzBuzz(FizzBuzzModel fizzBuzzModel)
        {
            IEnumerable<IFizzBuzzItem> fizzBuzzItems = null;
            int pageSize = ConfigSettings.PageSize;
            int pageIndex = 1;                                           
            fizzBuzzItems = fizzBuzzModel.GetFizzBuzz();
            ViewBag.FizzBuzzCount = fizzBuzzModel.FizzBuzzCount;
            
            return View("FizzBuzzView", fizzBuzzItems.ToPagedList<IFizzBuzzItem>(pageIndex, pageSize)); // View FizzBuzz Result for the first Page
        }


        /// <summary>
        /// Gets FizzBuzz result for the selected page.
        /// </summary>
        /// <param name="page">Integer</param>
        /// <param name="FizzBuzzCount">Integer</param>
       
        public ActionResult Paging(int? page, int? FizzBuzzCount)
        {
            IEnumerable<IFizzBuzzItem> fizzBuzzItems = null;
            int pageSize = ConfigSettings.PageSize;
            int pageIndex = 1;
                          
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            FizzBuzzModel model = new FizzBuzzModel();
            model.FizzBuzzCount = FizzBuzzCount.Value;
            fizzBuzzItems = model.GetFizzBuzz();
            ViewBag.FizzBuzzCount = FizzBuzzCount.Value;
            
            return View("FizzBuzzView", fizzBuzzItems.ToPagedList<IFizzBuzzItem>(pageIndex, pageSize)); // View the page selection changes
        }
                
    }
}
