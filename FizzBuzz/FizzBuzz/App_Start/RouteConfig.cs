﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FizzBuzz
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "",
                defaults: new { controller = "FizzBuzz", action = "Index", id = UrlParameter.Optional }
            );

            routes.MapRoute(
               name: "FizzBuzz",
               url: "FizzBuzz/GetFizzBuzz",
               defaults: new { controller = "FizzBuzz", action = "GetFizzBuzz", id = UrlParameter.Optional }
           );

            routes.MapRoute(
             name: "Paging",
             url: "FizzBuzz/Paging/{Page}",
             defaults: new { controller = "FizzBuzz", action = "Paging", page = UrlParameter.Optional }
             );

            routes.MapRoute(
               name: "Next",
               url: "FizzBuzz/Next",
               defaults: new { controller = "FizzBuzz", action = "Next", currentPageIndex = "1" }
           );

            routes.MapRoute(
              name: "Previous",
              url: "FizzBuzz/Previous",
              defaults: new { controller = "FizzBuzz", action = "Previous", currentPageIndex = "1" }
          );
        }
    }
}