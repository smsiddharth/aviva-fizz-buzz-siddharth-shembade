﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
namespace FizzBuzz.Models
{
    public class FizzBuzzItem :IFizzBuzzItem
    {
        public FizzBuzzItem(int fizzBuzzNumber, string displayText, Color color )
        {
            Index = fizzBuzzNumber;
            Text = displayText;
            this.Color = color;
        }

        public string Text { get; private set; }
        public Color Color { get; private set; }
        public int Index { get; private set; }
    }
}