﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace FizzBuzz.Models
{
    public  static class ConfigSettings
    {
        public static int FizzBuzzMinValue
        {
            get { return int.Parse(ConfigurationManager.AppSettings["FizzBuzzMinValue"]); }
        }

        public static int FizzBuzzMaxValue
        {
            get { return int.Parse(ConfigurationManager.AppSettings["FizzBuzzMaxValue"]); }
        }

        public static int PageSize
        {
            get { return int.Parse(ConfigurationManager.AppSettings["PageSize"]); }
        }
    }
}