﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.ComponentModel.DataAnnotations;

namespace FizzBuzz.Models
{
    public class FizzBuzzModel
    {
        public FizzBuzzModel()
        {

        }

        /// <summary>
        /// Gets the FizzBuzz result
        /// </summary>
        /// <returns>IFizzBuzzItem Collection</returns>
        public IEnumerable<IFizzBuzzItem> GetFizzBuzz()
        {
            if (FizzBuzzCount < ConfigSettings.FizzBuzzMinValue || FizzBuzzCount > ConfigSettings.FizzBuzzMaxValue)
            {
                throw new InvalidOperationException();
            }
            List<IFizzBuzzItem> fizzBuzz = new List<IFizzBuzzItem>();
            int i;
            bool isWednesday = false;
            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
            {
                isWednesday = true;
            }
            for (i = 1; i <= FizzBuzzCount; i++)
            {
                bool isFizz = false;
                bool isBuzz = false;               
                IFizzBuzzItem item = null;
                string fizzOrBuzz = string.Empty;
                if(i % 3 == 0)
                {
                    if (isWednesday)
                    {
                         item = new FizzBuzzItem(i, "wizz", Color.Blue);
                    }
                    else
                    {
                        item = new FizzBuzzItem(i, "fizz", Color.Blue);
                    }
                    isFizz = true;
                }

                if(i % 5 == 0)
                {
                    if (isWednesday)
                    {
                         item = new FizzBuzzItem(i, "wuzz", Color.Green);
                    }
                    else
                    {
                        item = new FizzBuzzItem(i, "buzz", Color.Green);
                    }
                   
                    isBuzz = true;
                }

                if (isFizz && isBuzz)
                {
                    if (isWednesday)
                    {
                        item = new FizzBuzzItem(i, "wizz wuzz", Color.Black);
                    }
                    else
                    {
                        item = new FizzBuzzItem(i, "fizz buzz", Color.Black);
                    }                    
                }

                if(!isFizz && !isBuzz)
                {
                    item = new FizzBuzzItem(i,i.ToString(), Color.Black);
                }

                fizzBuzz.Add(item);
            }

            return fizzBuzz.AsEnumerable<IFizzBuzzItem>();
        }

        [Required]
        [Range(1,900)]
        public int FizzBuzzCount { get; set; }
        
        
    }
   
}