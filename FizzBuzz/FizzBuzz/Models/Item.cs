﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Models
{
    public class Item : IItem
    {
        public Item(int Index)
        {
            this.Index = Index;
        }

        public int Index { get; private set; }
    }
}
