﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace FizzBuzz.Models
{
    public interface IFizzBuzzItem: IItem
    {
        string Text { get;  }
        Color Color { get;  }
        

    }
}
