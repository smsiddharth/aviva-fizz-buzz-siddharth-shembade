﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FizzBuzz.Controllers;
using FizzBuzz.Models;
using System.Web.Mvc;
namespace FizzBuzz.Tests
{
    [TestClass]
    public class FizzBuzzControllerTest
    {
        [TestMethod]
        public void IndexTest()
        {
            FizzBuzzController controller = new FizzBuzzController();
            ViewResult actionResult = (ViewResult) controller.Index();
            Assert.AreEqual("DefaultView", actionResult.ViewName);            
        }

        [TestMethod]
        public void GetFizzBuzzTest()
        {
            FizzBuzzController controller = new FizzBuzzController();           
            FizzBuzzModel model = new FizzBuzzModel();
            model.FizzBuzzCount = 100;
            ViewResult actionResult = (ViewResult)controller.GetFizzBuzz(model);
            Assert.AreEqual("FizzBuzzView", actionResult.ViewName);   
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetFizzBuzzFailedTest()
        {
            FizzBuzzController controller = new FizzBuzzController();            
            FizzBuzzModel model = new FizzBuzzModel();
            model.FizzBuzzCount = 0;
            ViewResult actionResult = (ViewResult)controller.GetFizzBuzz(model);
            Assert.AreEqual("FizzBuzzView", actionResult.ViewName);
        }

        [TestMethod]
        public void GetPagingTest()
        {
            FizzBuzzController controller = new FizzBuzzController();            
            ViewResult actionResult = (ViewResult)controller.Paging(2, 100);
            Assert.AreEqual("FizzBuzzView", actionResult.ViewName);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetPagingFailedTest()
        {
            FizzBuzzController controller = new FizzBuzzController();         
            ViewResult actionResult = (ViewResult)controller.Paging(2,0);
            Assert.AreEqual("FizzBuzzView", actionResult.ViewName);
        }
    }
}
